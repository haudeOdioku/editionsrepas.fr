---
Edition: Editions REPAS
DépotLégal: 2009
Auteur: Christophe Beau
Titre: La Danse des Ceps
SousTitre: Chronique de vignes en partage
Collection: Collection Pratiques Utopiques
ISBN : 978-2-9520180-8-1
Pages: 140 pages
Prix: 15
Etat: Disponible
Résumé: |
    Les Cepatou consommateurs de vins et "locataires de ceps", Philomène, Momo et bien d'autres, sont les "héros" de cette chronique qui se lit comme l'on boit un bon canon de vin l'été ! C'est l'histoire au fil des saisons, d'un vigneron qui a choisi une autre poésie du vin, une façon d'envisager son métier loin des tentations technologiques superflues, de soigner la vigne par des pratiques de bon sens et une agriculture biodynamique sans dogmatisme, de vivre un lien producteur-consommateurs dépouillé des habits de subjectivité parfois propres au monde du vin, de décliner une présence positive au terroir au delà des images surannées et des spéculations foncières banalisées.

    Un périple clochemerlesque qui vous mènera jusqu'au Mexique ou en Palestine. Découvrez y, autant l'ivresse des bons vins ou les bons moments de vendanges collectives, que des outils pratiques pour d'autres modèles de production agricole.
Tags:
- Agriculture biologique
- Collectif
- Alternatives
- Vin
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/la-danse-des-ceps-christophe-beau?_ga=2.198381743.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782952018081-la-danse-des-ceps-chronique-de-vignes-en-partage-2e-edition-christophe-beau/
SiteWeb: https://www.bogusateliers.wordpress.com
Couverture:
Couleurs:
  Fond: '#FCD5E6'
  Titre: '#F4AE1A'
  PageTitres: '#F4AE1A'
Adresse: 30260 Corconne
Latitude: 43.866978
Longitude: 3.933248
---

## L'auteur

Grand voyageur malgré ses pieds bien arrimés dans son vignoble gardois, **Christophe Beau** a toujours eu une main à la charrue et l'autre à la plume - l'une valant l'autre, nous a appris Rimbaud. C'est ainsi qu'il est également l'auteur de « Méditerranée : les jardiniers de l'avenir » (avec des photos de Marc Léger, préface de Pierre Rabhi, Ed 3 Spirales, 2005) et « Viva la pinata » (avec des photos de Marc Léger, Ed Le Lutin malin, 2007).

## Extrait

> À cette saison, les froids nocturnes riment avec de belles chaleurs d’après-midi. C’est une alternance merveilleuse pour la maturité des raisins. Avant de quitter je vais remettre mon petit nouveau groupe froid de la cave en route. Purger, cliqueter... c’est un peu du bricolage mon climatiseur, mais efficace pour couver les bouteilles assoupies en cave. À être au frais dans le cellier, je prends plaisir à regarder les piles de caisses classées par cru et par millésime. Sur trente mètres carrés, c’est un vrai labyrinthe de souvenirs de vendanges. Seul, je m’offre le bonheur discret de dévisager ces gisants heureux. Mais soudain, l’urgence de devoir remplir ma déclaration de stock avant la fin du mois, m’envahit. Hauteur par largeur et profondeur, plus des petits correctifs pour tenir compte des creux dans les piles. Et hop, voilà conclue en dix minutes cette déclaration. Je note cela sur le carton à l’entrée.
> -- page 12

## Le commentaire des éditeurs

Des vendanges collectives ; une toute petite exploitation… rentable ; un suivi du produit depuis le vignoble jusqu'aux bars à vins de la capitale et d'Amérique du Nord… Comme le dit dans sa préface Nicolas Duntze, porte-parole de la Confédération Paysanne :

> Ce témoignage vient illustrer très concrètement et humainement les réflexions et actions engagées partout sur le territoire en ce qui concerne les démarches "innovantes" d'installation agricole, les liens à créer entre les acteurs, et les outils de développement économique. Son intérêt réside aussi dans le témoignage de la présence, à chaque instant de nombreuses difficultés de tous ordres et des efforts communs à accomplir pour que la compréhension et la communication permettent de les résoudre collectivement. La narration d'une grande pudeur, de Christophe Beau, qui sait derrière ses lunettes et ses éprouvettes, rester discret, a une réelle vertu pédagogique et devrait permettre à certains d'imaginer et innover dans leurs parcours et leurs résistances à l'exclusion.

L'auteur nous le dira avec ses mots, poétiques et plein d'humour, à sa manière, toute particulière :

> Cep-atout ; ou l'atout cep; car nos consommations petites ou grandes passent par eux. Le cep, c'est le fruit du breuvage, mais c'est aussi l'unité de compte des rapports marchands avec le contrat de location "cepatou". Mais "c'est pas tout"; s'il n'y avait que cela ! Nos ceps, c'est le raisin et le vin, d'accord. Mais c'est aussi la danse de la taille (…autour du cep) l'hiver. C'est surtout une renaissance chaque printemps. Et puis; c'est la sieste de mûrissement l'été, c'est le chant d'automne à l'ombre du pressoir… Rien cependant n'aurait de sens sans le "sait pas tout". Car le cep, la vigne, la terre, le terroir, seraient ternes s'ils n'apprenaient au quotidien, comme tant de choses, qu'on ne connaît jamais tout, que chaque jour, chaque cep est un nouvel apprentissage, un éternel co-naitre, un droit à l'erreur.

## Du même auteur...

* *Pour Quelques Hectares de Moins. Tribulations coopératives d'un vigneron nomade*, Christophe Beau, 2011, Editions REPAS. [Voir la fiche du livre](/catalogue/pour-quelques-hectares-de-moins/)
